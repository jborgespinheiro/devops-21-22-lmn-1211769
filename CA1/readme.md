>> # DEVOPS 

> ## CA1 (class assignment 1)

![](vcs.png)

>### Parte 1

### Objectivo

Através do terminal, e **recorrendo ao software Git** que é um software de controlo de versões, gerir um conjunto de ficheiros
e hospedar as suas diferentes versões num repositório do Bitbucket previamente criado.
Esta primeira parte será executada com recurso a apenas uma **branch**.

>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.

> **Passo 1.2** - Instalação e configuração do Git

Executar o comando:

`§ git --version`

Se nenhuma versão estiver instalada, aparecerá uma solicitação para o fazer. Depois de instalada, configurar as credenciais
para identificação do user no git.

Executar os seguintes comandos:

`§ git config --global user.name “Joana Pinheiro [1211769]”`

`§ git config --global user.email 1211769@isep.ipp.pt`

Para confirmar que as credenciais foram guardadas, executar o comando:

`§ git config -l`

> **Passo 1.3** - Configuração do editor de texto a utilizar

Executar o comando:

`§ git config --global core.editor nano`

Desta forma, fica definido que o editor de texto utilizado será o nano.


> **Passo 1.4** - Criação de diretório CA1 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando 

`§ cd devops-21-22-lmn-1211769/`

`§ mkdir CA1`

Para criar a pasta CA1, é necessário entrar na pasta devops-21-22-lmn-1211769/ através do primeiro comando. Através
do segundo comando cria-se CA1

> **Passo 1.5** - Criação de ficheiro readme.md

Executar o comando:

`§ cd CA1/`

Uma vez executado este comando, entra-se no diretório CA1. O passo seguinte é a criação do ficheiro através do comando

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma. Para sair pressionar ^X e seguir as 
instruções para gravar as alteerações.

> **Passo 1.6** - Cópia do diretório /tut-react-and-spring-data-rest/  e todo o seu conteúdo 
> (*para efeitos deste tutorial, assume-se que este diretório já está previamente criado dentro da pasta /devops-21-22-lmn-1211769*)

Para voltar à pasta /devops-21-22-lmn-1211769, executar o comando:

`§ cd ..`

Para copiar um diretório para o interior de outro (e todo o seu conteúdo), executar o comando:

`§ cp -R tut-react-and-spring-data-rest CA1/`

- cp - comando para copiar
- R - para que a cópia seja recursiva
- < pasta a copiar >  para  < pasta de destino >

> **Passo 1.7** - Criação de issues no BitBucket (*Opcional*)

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.


> **Passo 1.8** - Adição das alterações realizadas ao repositório no BitBucket

Para adicionar as alterações feitas localmente ao repositório do bitbucket, executar o comando:

`§ git add .`

Este comando irá enviar os ficheiros para a ***staging area***. 

A inclusão do ponto final a seguir ao `add` é uma forma de adicionar todas as alterações efetuadas, de uma só vez, à staging area.

Posteriormente ao executar o comando:

`§ git status`

Havendo alterações ao diretório, no terminal irá aparecer a lista de alterações efectuadas iniciada pela seguinte mensagem:

    Changes to be committed:
    (use "git restore --staged <file>..." to unstage)

Nota: Os ***untracked files*** não aparecem na ***staging area*** se não forem adicionados previamente à mesma.
Para que isso aconteça deve ser executado novamente o comando 

`§ git add .`


As alterações estão prontas para serem enviadas para o repositório e ficarem acessíveis remotamente.

Aplicando o comando:

`§ git commit -m “message resolves #1”`

- commit - comando para enviar nova versão dos ficheiros
- -m - atráves deste comando é possível associar uma mensagem com descrição do commit a efetuar.

De seguida, executar o comando:

`§ git push`

- push - comando para efetuar a partilha a nova versão dos ficheiros alterados com o bitBucket.

Quando as alterações enviadas para o repositório têm especial importância, 
o git permite criar um ***tag*** para que seja posteriormente de fácil identificação.

Para criar um ***tag***, executar o seguinte comando:

`§ git tag v1.1.0`

Para associar o tag  ao último commit feito, executar o comando:

`§ git push origin v1.1.0`


O git também permite posteriormente aceder às informações desse tag como por exemplo: 
quem o criou, em que data e o seu conteúdo. Para esse efeito, executar o comando:

`§ git show v1.1.0`


> **Passo 1.9** - Implementação de uma nova funcionalidade no sistema - jobYears

Num IDE à escolha (neste exemplo, foi escolhido o IntelliJ) foi adicionado o novo campo ***jobYears*** e foram feitos 
testes para confirmar a nova implementação.
Feitas as alterações, volta-se ao terminal para enviar as alterações para o repositório remoto.


> **Passo 1.10** - Teste e debug através do spring-boot

Para executar este passo corretamente, é necessário garantir que se está no diretório correto.

Executar o comando:

`§ cd tut-react-and-spring-data-rest/basic/`

Após a execução do comando acima descrito, é possível correr o sprin-boot para testar as alterações 

`§ ./mvnw spring-boot:run`

Para fazer debug ao sistema acede-se ao link http://localhost:8080/ e com recorre-se à extensão ***React Developer Tools***


> **Passo 1.11** - Adicionar a nova funcionalidade ao repositório no BitBucket

Nota: Antes de enviar as alterações para a ***staging area**, será boa prática aplicar o comando:

`§ git status`

Desta forma, temos a informação de quais os ficheiros é que serão adicionados ao repositório remoto no próximo commit

Neste caso em concreto, foi detectado um novo ficheiro **EmployeeTest.java** como ***Untracked file***

Para que passar este ficheiro para a ***staging area*** executar o comando:

`§ git add .`

(Executar `§ git status` para confirmar que a operação foi bem sucedida)

Posteriormente voltar a executar os seguintes comandos (já explicados anteriormente neste documento):

`§ git commit -m "add a new field named JobYears resolves #4"`

`§ git push`

`§ git tag v1.2.0`

`§ git push origin v1.2.0`

`§ git tag ca1-part1`

`§ git push origin ca1-part1`

Desta forma fica concluída a primeira parte do CA1.

----------------------------------

>### Parte 2

### Objectivo

Continuar a gerir versões através do Git, mas desta vez, com recurso a mais que uma branch.

> **Passo 2.1** - Criação de uma nova branch que irá contemplar a nova funcionalidade do sistema

Executar o comando:

`§ git branch email-field`

desta forma fica criada uma nova branch com o nome "email-field"

Para confirmar que a branch foi criada, recorre-se ao comando:

`§ git branch`

Através deste comando, são devolvidas as branch existentes no repositório e respetivos nomes e marcado com um asterísco a branch ativa.


> **Passo 2.2** - Mudança para a branch recentemente criada

`§ git checkout email-field`

> **Passo 2.3** - Implementação de uma nova funcionalidade no sistema - email-field

Novamente, recorrendo ao IDE IntelliJ, fez-se a implementação da nova funcionalidade do sistema
e os testes a cobrir este novo campo.

> **Passo 2.4** - Adição das alterações realizadas ao repositório no BitBucket

Executar os seguintes comandos:

`§ git add .`

`§ git commit -m "add a new email field to the application resolves #5"`

`§ git push origin email-field`

Desta forma as alterações foram enviadas e registadas na nova branch - email-field.


> **Passo 2.5** - Teste e debug através do spring-boot

Para executar este passo corretamente, é necessário garantir que se está no diretório correto.

Executar o comando:

`§ cd tut-react-and-spring-data-rest/basic/`

Após a execução do comando acima descrito, é possível correr o sprin-boot para testar as alterações

`§ ./mvnw spring-boot:run`

Para fazer debug ao sistema acede-se ao link http://localhost:8080/ e com recorre-se à extensão ***React Developer Tools***

> **Passo 2.6** - Voltar à branch principal

`§ git checkout master`

> **Passo 2.7** - Consulta de todos os commits feitos até então

`§ git log `

> **Passo 2.8** - União da email-field branch com a master branch

`§ git merge email-field`

Depois de efetuar o merge, caso haja alguma alteração em ficheiros antes de executar o comando push,
haverá necessidade de executar os comandos:

`§ git add .`

`§ git commit -m " solve the merge conflict on readme.md file"`

> **Passo 2.9** - Resolução dos conflitos e envio das alterações para o repositório no Bitbucket

``§ git push origin master``

Neste caso, houve uma atualização do ficheiro readme.md e o merge da branch email-field com a branch master.

> **Passo 2.10** - Criação de tag para marcar esta última alteração

``§ git tag v1.3.0``

``§ git push origin v1.3.0``


> **Passo 2.11** - Criação de uma nova branch

`§ git branch fix-invalid-email`

``§ git branch``

> **Passo 2.12** - Implementação de melhoria na funcionalidade: email

Novamente, recorrendo ao IDE IntelliJ, aplica-se a nova condição solicitada:
o email tem de conter o símbolo @. 

> **Passo 2.13** - Envio das alterações para o repositório no Bitbucket

`§ git checkout fix-invalid-email`

`§ git add .`

`§ git commit -m "fix email method to ensure that it has an @"`

``§ git push origin fix-invalid-email``

> **Passo 2.14** - Voltar à branch principal

`§ git checkout master`


> **Passo 2.15** - União da fix-invalid-email branch com a master branch

`§ git merge fix-invalid-email`



> **Passo 2.16** - Resolução dos conflitos e envio das alterações para o repositório no Bitbucket

``§ git push origin``


> **Passo 2.17** - Criação de tag para marcar esta última alteração

``§ git tag v1.3.1``

``§ git push origin v1.3.1``

> **Passo 2.18** - Remoção das branches fix-invalid-email e email-field.

Para remover as branches que são fundidas à master branch, executar o comando

`§ git branch -d email-field`

`§ git push origin --delete email-field`

`§ git branch -d fix-invalid-email`

`§ git push origin --delete fix-invalid-email`


> **Passo 2.19** - Criação da tag ca1-part2


`§ git tag ca1-part2`

`§ git push origin ca1-part2`

----------------------------

> # Alternativa ao git

Na alternativa será feito o controlo de versões com auxílio do Mercurial.
Uma vez que este controlador de versões de software deixou de ser compatível com o 
repositório web Bitbucket, será usado o SourceForge para o efeito.

> Passo A.1 - Criar conta no SourceForge

Criar conta e posteriormente criar um projeto no repositório web.

> Passo A.2 - Clonar o repositório criado no SourceForge para a máquina local

Executar o comando:

`§ hg clone hg clone https://jborgespinheiro@hg.code.sf.net/p/devops-21-22-lmn-1211769/mercurial devops-21-22-lmn-1211769-mercurial`

Agora que o clone está concluído, mudar o diretório para:

`§ cd devops-21-22-lmn-1211769-mercurial`

> Passo A.3 - Criar um qualquer ficheiro no diretório

Executar o comando:

``§ touch README``

Com o ficheiro criado, executar os seguintes comandos.


> Passo A.4 - Adicionar as alterações ao repositório remoto

Adicionar o ficheiro à staging area:

`§ hg add `

Commit com a mensagem "Initial commit" e envio da alteração para o repositório remoto:

`§ hg commit -m 'Initial commit'`

`§ hg push`

> Passo A.5 - Copiar o diretório  Tutorial React.js and Spring Data REST Application.

Executar o comando:

`§ cp -R  ~/devops-21-22-lmn-1211769/CA1/tut-react-and-spring-data-rest/ ~/devops-21-22-lmn-1211769-mercurial/CA1`

Através deste comando faz-se a cópia de um diretório para outro [~/pasta de origem] para a [~/pasta de destino]


> Passo A.6 - Adicionar as alterações ao repositório remoto

`§ hg add`

Commit com a mensagem indicada e envio da alteração para o repositório remoto:

`§ hg commit -m "copy the CA1 folder"`

`§ hg push`

> Passo A.7 - Criar uma nova branch email-field

``§ hg branch email-field``

``§ hg push -f`` (foi necessário forçar o push de acordo com as sugestões do mercurial)

Antes de executar a merge entre as duas branches, foi feita uma alteração no ficheiro README criado no passo A.3

Para isso abre-se o ficheiro através do comando

`§ vim README`

De seguida adiciona-se uma alteração ao texto já existente (por ex: testing to generate a conflit when merge occurs)

Para sair do editor digitar `:wq`

> Passo A.8 - Merge das duas branches

`§ hg merge `

> Passo A.9 - Criar tag para associar ao commit feito

``§ hg tag v1.3.1``

`§ hg push`


conclusão:

Na minha perspetiva, apesar de os comandos serem muito semelhantes, o git é mais utilizado e isso acaba por
gerar mais suporte na web pela simples razão de haver mais utilizadores.


Um dos contras que tem o Mercurial é que não é suportado pelo bitbucket, o que creio que possa ser um fator contra na hora de o usar
uma vez que o SourceForge é um pouco menos intuitivo.

De qualquer forma, é extremamente positivo não ficarmos presos a uma só ferramenta e por isso é importante estarmos atentos
aos softwares disponíveis no mercado. 

No decorrer deste trabalho, pesquisei outras alternativas tais como o subversion ou o perforce.

Joana Pinheiro 
------------








