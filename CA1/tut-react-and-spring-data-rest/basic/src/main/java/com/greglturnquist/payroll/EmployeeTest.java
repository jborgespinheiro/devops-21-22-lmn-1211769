package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class EmployeeTest {

    @Test
    void getFirstName() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "explorer", 100, "littlehobbit@email.com");
        String name = "Frodo";
        String result = employee.getFirstName();
        assertEquals(name, result);
    }


    @Test
    void getFirstNameFailNullFirstName() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee(null, "Baggins",
                    "Hobbit", "explorer", 100, "littlehobbit@email.com");
        });
        //Assert
        assertEquals("First name can't be null.", thrown.getMessage());
    }

    @Test
    void getLastName() {
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "explorer", 100, "littlehobbit@email.com");
        String name = "Baggins";
        String result = employee.getLastName();
        assertEquals(name, result);
    }

    @Test
    void getLastNameFailNullLastName() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", null,
                    "Hobbit", "explorer", 100, "littlehobbit@email.com");
        });
        //Assert
        assertEquals("Last name can't be null.", thrown.getMessage());
    }

    @Test
    void getDescription() {

        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "explorer", 100, "littlehobbit@email.com");
        String description = "Hobbit";
        String result = employee.getDescription();
        assertEquals(description, result);
    }

    @Test
    void getDescriptionFailNullDescription() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    null, "explorer", 100, "littlehobbit@email.com");
        });
        //Assert
        assertEquals("Description can't be null.", thrown.getMessage());

    }

    @Test
    void getJobTitle() {
        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "explorer", 100, "littlehobbit@email.com");
        String jobTitle = "explorer";
        //Act
        String result = employee.getJobTitle();
        //Assert
        assertEquals(jobTitle, result);
    }

    @Test
    void getJobTitleFailNullJobTitle() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins",
                    "Hobbit", null, 100, "littlehobbit@email.com");
        });
        //Assert
        assertEquals("Job title can't be null.", thrown.getMessage());
    }


    @Test
    void getJobYears() {
        //Arrange
        Employee employee = new Employee("Frodo", "Baggins",
                "Hobbit", "explorer", 100, "littlehobbit@email.com");
        int jobYears = 100;
        //Act
        int result = employee.getJobYears();
        //Assert
        assertEquals(jobYears, result);
    }

    @Test
    void getJobYearsFailZeroYears() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins", "Hobbit", "explorer", 0,
                    "littlehobbit@email.com");
        });
        //Assert
        assertEquals("Job years must be positive.", thrown.getMessage());
    }


    @Test
    void getJobYearsFailNegativeYears() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins", "Hobbit", "explorer", -1,
                    "littlehobbit@email.com");
        });
        //Assert
        assertEquals("Job years must be positive.", thrown.getMessage());

    }

    @Test
    void getEmail() {
        Employee employee = new Employee("Frodo", "Baggins", "Hobbit", "two towers explorer",
                5, "littlehobbit@email.com");
        String expected = "littlehobbit@email.com";
        String result = employee.getEmail();
        assertEquals(expected, result);
    }


    @Test
    void getEmailFailNullEmail() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins", "Hobbit", "two towers explorer", 5, null);
        });
        //Assert
        assertEquals("Invalid email.", thrown.getMessage());
    }


    /*@Test
    void getEmailFailNullEmail2() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Arrange
            new Employee("Frodo", "Baggins", "Hobbit", "two towers explorer", 5, "littlehobbitandsam.twotowers.com");
        });
        //Assert
        assertEquals("Invalid email.", thrown.getMessage());
    }*/



}