>> # DEVOPS

> ## CA2 (class assignment 2)


>### Parte 1

Gradle software

>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA2 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando:

`§ cd devops-21-22-lmn-1211769/` - Este comando dá acesso ao repositório pretendido.

`§ mkdir CA2` - Criação de novo diretório CA2 com finalidade de guardar todos os ficheiros necessários para
este exercício.

`§ cd CA2` - Para entrar na pasata acabada de criar. 

`§ mkdir part1` - Criação de novo diretório part1 com a mesma finalidade já referida anteriormente.



> **Passo 1.3** - Criação de ficheiro readme.md

Depois de executado o último comando do passo 1.2, há condições para criar o ficheiro readme.md
que permitirá, ao longo do exercício, documentar todos os passos para a realização do mesmo.

Para o efeito, executar o comando:

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma.
Para forçar uma alteração ao ficheiro acabado de criar, introduz-se um simples texto "testing".

Para sair, pressionar ^X e seguir as instruções para gravar as alterações.


> **Passo 1.4** - Clone do repositório disponibilizado no BitBucket 

Aceder ao link disponibilizado para a resolução do exercício proposto:

 https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/

Clicar em CLONE e copiar o comando que aparecerá numa nova janela.

O comando que aparecerá é o:

`§ git clone https://jborgespinheiro@bitbucket.org/luisnogueira/gradle_basic_demo.git`

Este comando deverá ser executado no terminal dentro do diretório pertendido, que neste caso é o **../CA2/part1**.

Após clonar o repositório, deve ser removido o ficheiro .git para não gerar conflitos com o ficheiro original.

Aceder ao diretório /gradle_basic_demo através do comando:

`§ cd gradle_basic_demo`

Para remover o ficheiro .git, executar o comando:

`§ rm -rf .git` - é necessário aplicar o comando -rf para remover o ficheiro em questão, pois com apenas o -r não
foi possível.


> **Passo 1.5** - Criação de issues no BitBucket 

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.

Serão criadas issues para que as mesmas seja endereçadas aos commits feitos ao longo da resolução do exercício.


> **Passo 1.6** - Enviar alterações para o repositório remoto no Bitbucket


Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

`§ git add .`

`§ git status`

`§ git commit -m "CA2 and part1 folders created" `

`§ git push`

Por lapso, na mensagem do commit feito não foram associadas os issues relacionados.

Uma das formas para "corrigir" o commit feito é fazer uma alteração em qualquer dos ficheiros existentes. 
Assim sendo, foi feita uma pequena alteração ao ficheiro readme.md e dessa forma o Git deteta uma alteração entre
as duas versões do ficheiro e permite fazer novo commit.

`§ git add .`

`§ git status`

`§ git commit -m "folders created and readme file add to the folder part1 (resolves #6 , #7 and #8)"`

`§ git push`


> **Passo 1.7** - Instalação do software Gradle (*Opcional*)

Este passo é opcional uma vez que é possível executar todo o exercício proposto sem a instalação do Gradle, através
do wrapper.

De qualquer forma, para a instalação do software deve ser executado o comando na pasta gradle basic demo:

`§ brew install gradle`

> **Passo 1.8** - Adicionar a task executeServer ao ficheiro build.gradle

    
    task executeServer(type:JavaExec, dependsOn: classes) {

    group = "DevOps"

    description = "Start the server to comunicate with localhost: 59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
    }


> **Passo 1.9** - Build

De forma a confirmar que a task executeServer foi adicionada com sucesso, executar
o comando:

`§ gradle tasks`   

Irá aparecer uma lista de todas as tasks, inclusive a executeServer em DevOps tasks.

De seguida, para confirmar que a task está a compilar corretamente, executa-se o seguinte 
comando:

`§ gradle executeServer` 

Se a task correr com sucesso, irá aparecer a seguinte mensagem no terminal "BUILD SUCCESSFUL".

> **Passo 1.10** - Adicionar as últimas alterações ao repositório


`§ git add .`

`§ git status`

`§ git commit -m "task to run the server done resolves #9" `

`§ git push`

> **Passo 1.11** - Adicionar teste à App.Test

Seguindo as intruções dadas, é necessário criar o caminho de diretórios pretendido ->
src/test/java/basic_demo/AppTest.java

Para isso, é necessário aceder ao diretório ../CA2/part1/gradle_basic_demo/src

Posteriormente, através dos comandos ``§ mkdir`` e ``§ cd``, criar o caminho
test/java/basic_demo/

O ficheiro AppTest.java pode ser gerado automaticamente através do IDE.

Depois disto, é importante adicionar a dependência ao ficheiro gradle:
implementation 'org.junit.jupiter:junit-jupiter:5.7.0'


> **Passo 1.11** - Adicionar as últimas alterações ao repositório


`§ git add .`

`§ git status`

`§ git commit -m "created a unit test to the App.java and add depency to build.gradle resolves #10" `

`§ git push`


> **Passo 1.12** - Criar task CopySrc: criar um backup da pasta src (e respetivo conteúdo)
> para uma nova pasta previamente criada chamada backup

Aceder ao seguinte caminho (através do comando cd):

../part1/gradle_basic_demo

A task criada extende a task do tipo Copy e todas as suas características definidas no gradle.

Através do *from* indica-se a pasta que se pretende copiar para a pasta de destino backup através do comando *into*.

    task copySrc(type:Copy) {
    from ("src")
    into ("backup")
    }

> **Passo 1.13** - Build

De forma a confirmar que a task copySrc foi adicionada com sucesso, executar
o comando:

`§ gradle copySrc`

Se a task correr com sucesso, irá aparecer a seguinte mensagem 
no terminal "BUILD SUCCESSFUL".


> **Passo 1.14** - Adicionar as últimas alterações ao repositório

`§ git add .`

`§ git status`

`§ git commit -m "create a backup folder of the src resolves #11" `

`§ git push`

> **Passo 1.15** - Criar task zipfolder : copiar o conteúdo do diretório src e zippar a pasta

    task zipfolder(type: Zip) {
    from ("src")
    destinationDirectory = file ("backup")
    archiveName("zipfile");

A task criada extende a task do tipo Zip e todas as suas características definidas no gradle.

Através do *from* indica-se a pasta que se pretende executar o zip para a pasta de destino 
backup através do *destinationDirectory*. Por fim, através do comando *archiveName* define-se o nome da pasta zipada.


> **Passo 1.16** - Build

De forma a confirmar que a task zipfolder foi adicionada com sucesso, executar
o comando:

`§ gradle zipfolder`

Se a task correr com sucesso, irá aparecer a seguinte mensagem
no terminal "BUILD SUCCESSFUL".

> **Passo 1.17** - Adicionar as últimas alterações ao repositório

`§ git add .`

`§ git status`

`§ git commit -m "created a zip task and added the content of src folder to backup folder resolves #12" `

`§ git push`


>**Passo 1.18** - Criar a tag ca2-part1

No terminal, criar a tag pertendida:

`§ git tag ca2-part1`

`§ git push origin ca2-part1`

>**Passo 1.19** - Elaborar o ficheiro readme.md e enviar alterações para o repositório 

Através do terminal com recurso ao comando:

`§ nano readme.md`

ou 

num IDE à escolha, editar o documento readme.md criado previamente e elencar todos os pontos necessários
para a resolução do CA2-part1.

Agora é novamente possível enviar as alterações para o Bitbucket 

`§ git add .`

`§ git status`

`§ git commit -m "updated readme.md" `

`§ git push`

Uma vez que o ficheiro foi alterado posteriormente à criação da tag e uma vez que um tag é possível apontar para apenas um 
commit, uma opção possível para taggar a última versão para esta part1 é criar uma nova tag com o nome ca2-part1-v1.

No terminal, criar a tag pertendida:

`§ git tag ca2-part1-v1`

`§ git push origin ca2-part1-v1`





