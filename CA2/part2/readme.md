>> # DEVOPS

> ## CA2 (class assignment 2)


>### Parte 2

Gradle software - converter a pasta basic do Tutorial App que utiliza as dependências 
Maven para o software de compilação automática Gradle.

>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA2 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando:

`§ cd devops-21-22-lmn-1211769/` - Este comando dá acesso ao repositório pretendido.

`§ mkdir CA2` - Criação de novo diretório CA2 com finalidade de guardar todos os ficheiros necessários para
este exercício.

`§ cd CA2` - Para entrar na pasata acabada de criar.

`§ mkdir part2` - Criação de novo diretório part1 com a mesma finalidade já referida anteriormente.


> **Passo 1.3** - Criação de ficheiro readme.md

Depois de executado o último comando do passo 1.2, há condições para criar o ficheiro readme.md
que permitirá, ao longo do exercício, documentar todos os passos para a realização do mesmo.

Para o efeito, executar o comando:

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma.
Para forçar uma alteração ao ficheiro acabado de criar, introduz-se um simples texto "testing".

Para sair, pressionar ^X e seguir as instruções para gravar as alterações.

> **Passo 1.4** - Criação de issues no BitBucket

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.

Serão criadas issues para que as mesmas seja endereçadas aos commits feitos ao longo da resolução do exercício.


> **Passo 1.5** - Enviar alterações para o repositório remoto no Bitbucket


Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

`§ git add .`

`§ git status`

`§ git commit -m "part2 folder and readme.md file inside  CA2 resolves #13" `

`§ git push`

> **Passo 1.6** - Criar nova branch no repositório chamada tut-basic-gradle

`§ git branch tut-basic-gradle`

Para confirmar a criação da branch, executar o comando:

`§ git branch`

Para mudar para a branch recentemente criada, executar comando:

`§ git checkout tut-basic-gradle`

> **Passo 1.7** - Aceder ao link disponibilizado para criar um projeto gradle spring boot 

https://start.spring.io

Depois de gerar o projeto de acordo com as instruções indicadas, extrair a pasta zipada para dentro
do diretório ../CA2/part2

Executar o seguinte comando para confirmar as tasks disponíveis no projeto:

`§ gradle tasks`


> **Passo 1.8** - Apagar a pasta src do projeto acabado de criar

Aceder à pasta onde se encontra a src:

` § cd CA2/part2/react-and-spring-data-rest `

Executar o comando para apagar a pasta src:

`§ rm -rf src`


> **Passo 1.9** - Copiar a pasta src do basic tutorial

Para o efeito, acede-se ao repositório original e através do comando 

`§ git clone https://github.com/spring-guides/tut-react-and-spring-data-rest`

Este repositório foi clonado para uma pasta /temp para permitir copiar a pasta src mencionada e necessária para
avançar com o exercício.


Através do comando `§ cp `copia-se o diretório pertendido para a pasta de destino.
É necessário também copiar os ficheiros *webpack.config.js* e *package.json*

Não é possível apagar a pasta src/main/resources/static/built pois foi feito um clone do repositório original 
e a pasta, de momento, ainda não existe.

> **Passo 1.10** - Enviar alterações para o repositório remoto no Bitbucket 


Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

Enviar as alterações para a branch tut-basic-gradle:

`§ git add .`

`§ git status`

`§ git commit -m "correction of the folders that i cloned earlier and copy the src folder to the 
CA/part2/react-and-spring-data-rest-basic" `

`§ git push origin tut-basic-gradle`

> **Passo 1.11** - Build e bootRun

No diretório /CA2/part2/react-and-spring-data-rest-basic executar o comando para confirmar que o projeto faz build corretamente

``§ gradle build``

Posteriormente executar o comando

``§ gradle bootRun``

Conforme referido no documento, acedendo a http://localhost:8080 
percebe-se que não é possível aceder à página porque falta o plugin referente ao 
frontend.

> **Passo 1.12** - Adicionar o plugin de frontend e respetiva configuração

Introduzir o plugin no ficheiro gradle através do IDE:

    id "org.siouan.frontend-jdk11" version "6.0.0"

    frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run build"
    cleanScript = "run clean"
    checkScript = "run check"
    }
 
> **Passo 1.13** - No package.json adicionar o script

    "scripts": {
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",
    "test": "echo Testing frontend"
    },

> **Passo 1.14** - Build e bootRun

No diretório /CA2/part2/react-and-spring-data-rest-basic executar o comando para confirmar que o projeto faz build corretamente

``§ gradle build``

Posteriormente executar o comando

``§ gradle bootRun``

Desta vez, já é possível aceder à página web com sucesso devido às alterações feitas.

> **Passo 1.15** - Adicionar uma task copyJar

Esta task extende a task Copy que já está implementada no gradle tasks por defeito.

    task copyJar(type: Copy) {
    from 'build/libs/'
    into 'dist'
    }

> **Passo 1.16** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

Enviar as alterações para a branch tut-basic-gradle:

`§ git add .`

`§ git status`

`§ git commit -m "several changes to suport frontend. add task to copy .jar resolves #14" `

`§ git push origin tut-basic-gradle`



> **Passo 1.17** - Adicionar uma task deleteBuiltFiles

Esta task extende a task Delete que já está implementada no gradle tasks por defeito.

    task deleteBuiltFiles(type: Delete) {

	group = "DevOps"
	description = "deletes the files from built folder"

	delete "src/main/resources/static/built"
    }

    clean.dependsOn deleteBuiltFiles

Conforme é possível verificar, a task será exeutada antes da task clean pois esta última depende da primeira para correr.


> **Passo 1.18** - Enviar alterações para o repositório remoto no Bitbucket 

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

Enviar as alterações para a branch tut-basic-gradle:

`§ git add .`

`§ git status`

`§ git commit -m "task delete added resolves #15" `

`§ git push origin tut-basic-gradle`


> **Passo 1.19** - Correr as tasks criadas

``§ gradle copyJar``
``§ gradle deleteBuiltFiles``

Ao executar estas duas tasks o build foi feito com sucesso e como seria de esperar, foram
apagados os ficheiros built gerados.

> **Passo 1.20** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

Enviar as alterações para a branch tut-basic-gradle:

`§ git add .`

`§ git status`

`§ git commit -m "everything is working fine on gradle because all the tasks build sucessfully resolves #16" `

`§ git push origin tut-basic-gradle`


> **Passo 1.21** - Merge da branch criada para este propósito com a branch principal

``§ git checkout master``
``§ git merge tut-basic-gradle``

> **Passo 1.22** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):

Enviar as alterações para a branch master (uma vez que a outra branch já foi fundida
com a master)

`§ git add .`

`§ git status`

`§ git commit -m "nano readme.md file updated" `

`§ git push origin `



>### Alternativa - Compilador Automático Maven

> **Passo A.1** - Criação do diretório alternativa dentro do diretório /devops-21-22-lmn-1211769/CA2

Executar o comando:

`§ cd devops-21-22-lmn-1211769/CA2` - Este comando dá acesso ao repositório pretendido.

`§ mkdir alternativa` - Criação de novo diretório alternativa.




> **Passo A.1** - Aceder ao link disponibilizado para criar um projeto maven spring boot

https://start.spring.io

Depois de gerar o projeto de acordo com as instruções indicadas, extrair a pasta zipada para dentro
do diretório ../CA2/part2/alternativa


> **Passo A.2** - Apagar a pasta src do projeto acabado de criar

Aceder à pasta onde se encontra a src:

` § cd CA2/alternativa/react-and-spring-data-rest `

Executar o comando para apagar a pasta src:

`§ rm -rf src`



> **Passo A.3** - Copiar o conteúdo da pasta src do basic

Conforme já referido no ponto 1.9 deste documento, foi feito um clone do repositório pretendido para a pasta /temp

Através do comando `§ cp `copia-se o diretório pertendido para a pasta de destino - 
CA2/alternativa/react-and-spring-data-rest

Da mesma forma, é igualmente necessário copiar os ficheiros *webpack.config.js* e *package.json*.


Não é possível apagar a pasta src/main/resources/static/built pois foi feito um clone do repositório original
e a pasta, de momento, ainda não existe.


> **Passo A.4** - Correr a aplicação - bootRun

`§ ./mvnw springboot:run`

Tal como com o software Gradle, após inicializar o Maven, nada acontece acedendo a http://localhost:8080 pois também aqui
falta adicionar o plugin de frontend.

Ir a https://github.com/eirslett/frontend-maven-plugin e retirar os plugins necessários.



> **Passo A.5** - No package.json adicionar o script

    "scripts": {
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",
    "test": "echo Testing frontend"
    },

> **Passo A.6** - Correr a aplicação - build e bootRun

`§ ./mvnw install`

`§ ./mvnw springboot:run`

Tal como com o software Gradle, ao executar estes dois comandos, após as alterações feitas no projeto é possível verificar 
que o link http://localhost:8080 agora funciona.

> **Passo A.7** - Adicionar a task copyJar 

                <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.2.0</version>
				<executions>
					<execution>
						<id>copy-Jar</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>dist</outputDirectory>
							<resources>
								<resource>
									<directory>target</directory>
									<includes>
										<include>**/*.jar</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>


> **Passo A.8** - Adicionar a task delete

            <plugin>
                <artifactId>maven-delete-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>src/main/resources/static/built</directory>
                            <followSymlinks>false</followSymlinks>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>



> **Passo A.9** - Correr as tasks criadas

`§ ./mvnw install` - corre o build e copia a pasta src

`§ ./mvnw clean` - apaga os ficheiros built conforme era suposto

>**Passo A.10** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):


`§ git add .`

`§ git status`

`§ git commit -m "maven alternative implemented resolves #17" `

`§ git push`


>**Passo A.11** - Gradle Vs Maven

Como se sabe, existem vários processos essenciais para se consiga executar um sistema. 
Para isso, existem várias tarefas/processos que devemos considerar e no fundo, automatizar.

Daí ter sido desenvolvidos software de automatização de tarefas que na maior parte das vezes são repetitivas em qualquer
sistema.

Compilar (Build) é, neste caso, transformar o código Java num tipo que seja interpretado por qualquer outra máquina.


Maven:

– Utiliza XML

– Utiliza convenções, além disso considera convenção sobre configuração

– Por convenção o nome do arquivo de configuração se chama pom.xml

– Trabalha com Plugins

– Possui um ciclo de vida composto fixo e por goals padrões

– Estrutura de diretórios padrão



Gradle:

– Utiliza DSL baseada no Groovy

– Utiliza convenções, mas permite flexibilidade

– Por convenção o seu arquivo de configuração chama-se build.gradle

– Trabalha com Plugins

– Dispõe de tasks (tarefas) prontas e permite que se extenda novas versões a partir dessas tasks originais 

– Estrutura de diretórios padrão

– Altamente Configurável