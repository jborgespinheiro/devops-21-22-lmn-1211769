>> # DEVOPS

> ## CA4 (class assignment 1)


>### Parte 1A

Docker - criar um container onde seja possível correr a aplicação chatServer. Do lado do local host deverá correr 
a parte do cliente e a parte do chat server deverá correr dentro do container.


>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA4 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando:

`§ cd devops-21-22-lmn-1211769/` - Este comando dá acesso ao repositório pretendido.

`§ mkdir CA4` - Criação de novo diretório CA4 com finalidade de guardar todos os ficheiros necessários para
este exercício.

`§ cd CA4` - Para entrar na pasta acabada de criar.

`§ mkdir part1A` - Criação de novo diretório part1 com a mesma finalidade já referida anteriormente.


> **Passo 1.3** - Criação de ficheiro readme.md

Depois de executado o último comando do passo 1.2, há condições para criar o ficheiro readme.md
que permitirá, ao longo do exercício, documentar todos os passos para a realização do mesmo.

Para o efeito, executar o comando:

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma.

Para sair, pressionar ^X e seguir as instruções para gravar as alterações.

> **Passo 1.4** - Criação de issues no BitBucket

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.

Serão criadas issues para que as mesmas seja endereçadas aos commits feitos ao longo da resolução do exercício.

> **Passo 1.5** - Instalação do software Docker Desktop

Aceder ao link https://www.docker.com/products/docker-desktop/ 
e fazer download para versão macOS

> **Passo 1.6** - Criação do ficheiro Dockerfile

Criar um ficheiro e atribuir-lhe o nome de **Dockerfile** e descrever cada passo necessário para que a
imagem seja criada.

> **Passo 1.7** - Criação da imagem

Dentro do diretório CA4/part1A executar o comando

`§ docker build -t ca4part1imga .` onde,

- ca4part1imga é nome dado à imagem criada

> **Passo 1.8** - Criação de conta no Docker Hub

Aceder ao link https://hub.docker.com
e seguir os passos para criar a conta.

No terminal executar o comando,

`§ docker login` e inserir as credenciais 

> **Passo 1.9** - Criação de container

`§ docker run --name container_part1 -p 59001:59001 -d ca4part1imga` onde,

- --name é comando para atribuir o nome ao container a criar
- -p definição dos portos
- -d utilização da imagem ca4part1imga para criar o container

> **Passo 1.10** - Consulta das imagens e containers

`§ docker images` para consultar as imagens criadas


`§ docker ps ` para consultar os containers

> **Passo 1.10** - Carregamento da imagem criada no Docker Hub

Com o login já efetuado no passo 1.8, executar o comando 

`§ docker tag ca4part1imga joanapinheiro769/ca4part1imga` onde,

- ca4part1imga é imagem criada
- joanapinheiro769 é o docker hub id


`§ docker push joanapinheiro769/ca4part1imga`


> **Passo 1.11** - Correr a aplicação Chat Client na máquina local

Aceder ao diretório /gradle_basic_demo

Esta pasta encontra-se no caminho ../CA2/part1/gradle_basic_demo

`§ ./gradlew runClient`

Ao executar este comando, a máquina local comunica com o container a aparece a janela do chat, como seria de esperar. 


> **Passo 1.12** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):


`§ git add .`

`§ git status`

`§ git commit -m "updadted readme.md" `

`§ git push`

`§ git tag ca4-part1a`

`§ git push origin ca4-part1a`





