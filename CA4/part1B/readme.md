>> # DEVOPS

> ## CA4 (class assignment 1)


>### Parte 1B

Docker - criar um container onde seja possível correr a aplicação chatServer. Do lado do local host deverá correr 
a parte do cliente e a parte do chat server deverá correr dentro do container.


>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA4 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando:

`§ cd devops-21-22-lmn-1211769/CA4` - Este comando dá acesso ao repositório pretendido.


`§ mkdir part1B` - Criação de novo diretório part1 com a mesma finalidade já referida anteriormente.


> **Passo 1.3** - Criação de ficheiro readme.md

Depois de executado o último comando do passo 1.2, há condições para criar o ficheiro readme.md
que permitirá, ao longo do exercício, documentar todos os passos para a realização do mesmo.

Para o efeito, executar o comando:

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma.

Para sair, pressionar ^X e seguir as instruções para gravar as alterações.

> **Passo 1.4** - Criação de issues no BitBucket

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.

Serão criadas issues para que as mesmas seja endereçadas aos commits feitos ao longo da resolução do exercício.


> **Passo 1.5** - Clone do repositório 

Dentro do diretório de ..CA4/part1B, clonar o seguinte repositório:

`§ git clone https://jborgespinheiro@bitbucket.org/luisnogueira/gradle_basic_demo.git`

Aceder ao ../CA4/part1B/gradle_basic_demo através do comando ``§ cd`` e correr o seguinte 
comando:

`§ ./gradlew clean build`

> **Passo 1.6** - Criação do ficheiro Dockerfile

Criar um ficheiro e atribuir-lhe o nome de **Dockerfile** e descrever cada passo necessário para que a
imagem seja criada.

> **Passo 1.7** - Criação da imagem

Dentro do diretório CA4/part1B executar o comando

`§ docker build -t ca4part1imgb .` onde,

- ca4part1imgb é nome dado à imagem criada


> **Passo 1.8** - Criação de container

Dentro do diretório CA4/part1B, executar o comando,

`§ docker login` e inserir as credenciais da conta anteriormente criada

`§ docker run --name container_part2 -p 59001:59001 -d ca4part1imgb` onde,

- --name é comando para atribuir o nome ao container a criar
- -p definição dos portos
- -d utilização da imagem ca4part1imga para criar o container

> **Passo 1.9** - Consulta das imagens e containers

`§ docker images` para consultar as imagens criadas

`§ docker ps ` para consultar os containers

> **Passo 1.10** - Carregamento da imagem criada no Docker Hub

Com o login já efetuado no passo 1.7, executar o comando 

`§ docker tag ca4part1imgb joanapinheiro769/ca4part1imgb` onde,

- ca4part1imgb é imagem criada
- joanapinheiro769 é o docker hub id


`§ docker push joanapinheiro769/ca4part1imgb`


> **Passo 1.11** - Correr a aplicação Chat Client na máquina local

Aceder ao diretório /gradle_basic_demo (clonado com o propósito de resolver este exercício)

Esta pasta encontra-se no caminho ../CA4/part1B/gradle_basic_demo e executar:

`§ ./gradlew runClient`

Ao executar este comando, a máquina local comunica com o container a aparece a janela do chat, como seria de esperar. 


> **Passo 1.12** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos (já descritos no CA1 - readme.md):


`§ git add .`

`§ git status`

`§ git commit -m "updadted readme.md" `

`§ git push`

`§ git tag ca4-part1b`

`§ git push origin ca4-part1b`





