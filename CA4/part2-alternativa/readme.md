>> # DEVOPS

> ## CA4 (class assignment 4)


>### Parte 2 - Alternativa


## Opção 1

### Docker

O Docker é uma plataforma comercial de conteinerização e runtime que ajuda os desenvolvedores a criar, implantar e executar contentores.
Utiliza uma arquitetura cliente-servidor com comandos simples e automação por meio de uma única API.

O Docker também fornece um kit de ferramentas que é comummente usado para empacotar aplicações em imagens e grava um Dockerfile e executa
os comandos apropriados para criar a imagem usando o servidor Docker.
Embora o Docker forneça uma maneira eficiente de empacotar e distribuir contentores, executar e gerir contentores em escala 
torna-se difícil não recorrer a outras ferramentas para o fazer de maneira mais limpa.
Coordenar e agendar contentores em vários servidores/clusters, 
atualizar ou implantar aplicações com tempo de inatividade e monitorar a integridade dos contentores 
são apenas algumas das necessidades que precisam ser resolvidas.


Daí terem surgido soluções para orquestrar contentores na forma de Kubernetes, Docker Swarm, Mesos, HashiCorp Nomad e outros.
Isso permite que as organizações consigam gerir um grande volume de contentores e usuários, equilibrem cargas com eficiência,
ofereçam autenticação e segurança, implantação multiplataforma e muito mais.


### Kubernetes

Kubernetes é uma plataforma open-source que orquestra sistemas de tempo de execução de contentores num cluster de recursos de rede. 
O Kubernetes pode ser usado com ou sem o Docker.

O Kubernetes foi originalmente desenvolvido pela Google, que precisava de uma nova maneira de executar bilhões de contentores por semana 
em escala. O principal objetivo do Kubernetes é facilitar a implantação e o gestão de sistemas distribuídos complexos,
enquanto ainda se beneficia da utilização aprimorada que os contentores trouxeram.

O Kubernetes agrupa um conjunto de contentores num grupo que ele gere na mesma máquina para reduzir a sobrecarga da rede
e aumentar a eficiência do uso de recursos. 

O Kubernetes é particularmente útil para equipas de DevOps, pois oferece descoberta de serviço, equilíbrio de carga dentro do cluster,
rollouts e rollbacks automatizados, 
autorrecuperação de contentores que falham e gestão de configuração. 

No entanto, o Kubernetes não é uma plataforma completa como serviço (PaaS) e há muitas considerações a ter em conta ao criar e gerir clusters do Kubernetes.


### Diferenças entre Dockers e Kubernetes


Enquanto o Docker um contentor runtime, o Kubernetes é uma plataforma para executar e gerir contentores. 
O Kubernetes oferece suporte a vários contentor runtimes, incluindo Docker, containerd, CRI-O e qualquer implementação
do Kubernetes CRI (Container Runtime Interface). 
Uma boa metáfora é o Kubernetes como um “sistema operacional” e os contentores Docker são “aplicações” que instalamos no “sistema operacional”.

Por si só, o Docker é altamente benéfico para o desenvolvimento de aplicações modernas pois resolve o problema clássico 
de “funciona na minha máquina”, mas em mais nenhum lugar. A ferramenta de orquestração de contentores Docker Swarm é capaz 
de lidar com uma implantação de carga de trabalho de contentor de produção de alguns contentores. 
Quando um sistema cresce e precisa adicionar muitos contentores em rede, o Docker por si só, pode enfrentar alguns problemas 
de crescimento que o Kubernetes ajuda a resolver.

O Kubernetes orquestra clusters de máquinas para trabalharem juntos e agenda a execução de contentores nessas máquinas com base 
nos seus recursos disponíveis. Os contentores são agrupados, por meio de definição declarativa, em pods, que é a unidade 
básica do Kubernetes. O Kubernetes gere automaticamente coisas como descoberta de serviços, equilíbrio de carga, alocação 
de recursos, isolamento e dimensionamento de seus pods vertical ou horizontalmente. 


### Docker & Kubernetes - cooperação

Como referido em cima, o Kubernetes pode ser executado sem o Docker e o Docker pode funcionar sem o Kubernetes. 
Mas o Kubernetes pode (e beneficia) muito do Docker e vice-versa.

Tendo o Docker instalado em vários hosts (diferentes sistemas operativos), é possível potenciar a utilização de Kubernetes. 
Esses nós, ou hosts Docker, podem ser considerados máquinas virtuais. 
O Kubernetes permitir a automatização do provisionamento de contentores, rede, gestão de carga, segurança
e dimensionamento em todos esses nós a partir de uma única linha de comando ou painel. Uma coleção de nós que 
é gerenciada por uma única instância do Kubernetes chama-se cluster do Kubernetes.

Porque é necessário ter vários nós? 

Para tornar a infraestrutura mais robusta: A aplicação estará online, mesmo que alguns dos nós fiquem offline, 
ou seja, garante-se alta disponibilidade.
Para tornar a aplicação mais escalável: se a carga de trabalho aumentar, basta gerar mais contentores e/ou adicionar mais nós 
ao seu cluster Kubernetes.

### Kubernetes como alternativa ao Docker

Tal como o Docker recorre a um ficheiro docker-compose.yaml, ao utilizar Kubernetes devemos ter também um simple-pod.yaml.
E mais uma vez, tal como no software Docker, é possível definir um ou mais contentores no Pod.
Contudo, recorrendo ao Kubernetes é necessário configurar e correr um Cluster antes de executar esse Pod.
Um cluster contempla vários nós que hospedam os pods, e que por sua vez, inicializarão o(s) contentor(es).

Tal como no Docker, o Kubernetes tem um sistema semelhante para atribuir nomes de imagens criadas.