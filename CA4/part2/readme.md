>> # DEVOPS

> ## CA4 (class assignment 4)


>### Parte 2

Docker - criar um container onde seja possível correr a aplicação chatServer. Do lado do local host deverá correr
a parte do cliente e a parte do chat server deverá correr dentro do container.


>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA4/part2 dentro do diretório /devops-21-22-lmn-1211769

Executar o comando:

`§ cd devops-21-22-lmn-1211769/CA4` - Este comando dá acesso ao repositório pretendido.


`§ mkdir part2` - Criação de novo diretório part2 com a mesma finalidade já referida anteriormente.

`§ mkdir db` - Criar pasta dentro de /part2

`§ mkdir web` - Criar pasta dentro de /part2

`§ mkdir data` - Criar pasta dentro de /part2
`


> **Passo 1.3** - Criação de ficheiro readme.md

Depois de executado o último comando do passo 1.2, há condições para criar o ficheiro readme.md
que permitirá, ao longo do exercício, documentar todos os passos para a realização do mesmo.

Para o efeito, executar o comando:

`§ nano readme.md`

O editor de texto abrirá automaticamente. O documento está pronto a ganhar forma.

Para sair, pressionar ^X e seguir as instruções para gravar as alterações.

> **Passo 1.4** - Criação de issues no BitBucket

Os issues são uma forma de conseguir, através de um identificador numérico sequencial (#1, #2, #3, ….), acompanhar as alterações feitas.

Para criar um issue no bitBucket, acede-se ao repositório pretendido e na barra lateral clicar em “issues”.

De seguida, clica-se em “create issue”.

Preencher o campo *title*, *issue type* e a sua *priority*.

Serão criadas issues para que as mesmas seja endereçadas aos commits feitos ao longo da resolução do exercício.


> **Passo 1.5** - Copiar react-and-spring-data-rest-basic do repositório CA3/part2 para CA4/part2

Através do terminal, utilizado o comandos ``§ cd`` e ``§ cp``copiar a pasta pretendida de um local para o outro.


> **Passo 1.6** - Criar Dockerfiles nas pastas /db e /web

Criar um ficheiro e atribuir-lhe o nome de **Dockerfile** (em cada uma das pastas) e descrever cada passo necessário para que a
imagem seja criada.
Baseados nos Dockerfiles fornecidos, os Dockerfiles têm de ser alterados para que possam ficar compativeis com a aplicação.



No db/Dockerfile:

- Necessário alterar a versão do ubuntu para garantir que a mesma é compatível com
os restantes softwares, nomeadamente o jdk11.


No web/Dockerfile:

- Necessário alterar a versão do tomcat que seja compatível com jdk11 e com a versão spring
(tomcat:9.0-jdk11-temurin)
- o clone pretendido será o meu repositório
https://jborgespinheiro@bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769.git
- Alterar o WORKDIR para /tmp/build/devops-21-22-lmn-1211769/CA4/part2/react-and-spring-data-rest-basic
- dar premissões através de ``§ chmod u+x gradlew``
- Alterar o local de onde será feita a cópia
 ``§ cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps``


> **Passo 1.7** - Editar o docker-compose.yml dentro da CA4/part2 

Criar um ficheiro dentro do local pretendido  (CA4/part2) e, com base no docker-compose.yml disponibilizado, fazer as alterações
necessárias, nomeadamente os ipv4s e a subnet mask.



> **Passo 1.8** - Commit das alterações feitas

`§ git add .`

`§ git commit -m "Dockerfiles added and docker-compose.yml"`

`§ git push`

E posteriormente resolver a questão do gradle wrapper.

`§ git add -f gradle/wrapper/gradle-wrapper.jar`

`§ git commit -m "add gradle wrapper.jar"`

`§ git push`

Da mesma forma, forçar a adição do ficheiro .war para qua, quando corra o docker-compose a cópia seja feita.
desta forma estamos a contraiar o ficheiro .gitignore

``§ git add -f react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war``

``§ git commit -m "fix build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war path"``

`§ git push`

> **Passo 1.9** - Correr os container criados


`§ docker compose build`

`§ docker compose up` para que sejam inicializados os containers definidos.


> **Passo 1.10** - Testar a aplicação (web container e db container)


Abrir o URL

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ 

e como seria de esperar, aparece a seguinte tabela pois correu tudo conforme planeado

![](tabela.png)


Abrir o URL 

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

Inserir jdbc:h2:tcp://192.168.56.11:9092/./jpadb

INSERT INTO EMPLOYEE (ID, DESCRIPTION, EMAIL, FIRST_NAME, JOB_TITLE, JOB_YEARS,
LAST_NAME) VALUES (2, 'hobbit', 'hobbit@shire.com', 'sam', 'helper', 50,
'BagginsToBe')


Voltar a carregar o URL:

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

e perceber que as alterações feitas ficaram gravadas adicionando mais uma linha à tabela.


> **Passo 1.11** - Upload das imagens criadas no Docker Hub

`§ docker login`

``§ docker tag part2_db joanapinheiro769/part2_db``

`§ docker push joanapinheiro769/part2_db`

``§ docker tag part2_web joanapinheiro769/part2_web``

`§ docker push joanapinheiro769/part2_web`

> **Passo 1.12** - Copiar os ficheiros do container para a pasta data na máquina local


`§ docker compose up`

Posteriomente e com os containers a correr, abrir uma nova janela no terminal e executar o seguinte comando:

`` § docker-compose exec db bash``

``§ cp jpadb.mv.dv /usr/src/data-backup``


> **Passo 1.13** - Commit das alterações feitas

Abrindo uma nova janela no terminal e acedendo ao diretório ../CA4/part2


`§ git add .`

`§ git commit -m "Update readme file"`

`§ git push`

`§ git tag ca4-part2`

`§ git push origin ca4-part2`