>> # DEVOPS

> ## CA5 (class assignment 5)


>### Parte 1


## Jenkins

>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA5/part1 dentro do diretório /devops-21-22-lmn-1211769

Executar os comandos:

`§ cd devops-21-22-lmn-1211769` - Este comando dá acesso ao repositório pretendido.

`§ mkdir CA5` - Criação de novo diretório CA5.

`§ mkdir part1` - Criação de novo diretório part1.



> **Passo 1.3** - Instalar jenkins software


Através do link https://www.jenkins.io/download/ instalar o jenkins.war


> **Passo 1.4** - Correr o war file diretamente no terminal

``§ java -jar jenkins.war`` (depois de configurar que o java é o 11)

Depois de correr o comando, é criado um user com uma password associada.


> **Passo 1.5** - Aceder ao endereço

http://localhost:8080 e deixar instalar os plugins.

Depois será necessário criar uma conta para associar ao Jenkins.


Desta forma, os pluggins estão intalados e é possível ver o avanço da sua instalação através do terminal.


> **Passo 1.6** - Copiar gradle_basic_demo para a pasta CA5/part1 e guardar as alterações no repositório

Copiar a pasta gradle_basic_demo através do comando ``§ cp`` para a pasta CA5/part1

Posteriormente dentro da pasta CA5/part1/gradle_basic_demo executar os comandos:

``§ git add -f gradle-wrapper.jar ``

`§ git commit -m "forcing gradle-wrapper.jar"`

`§ git push`

`§ git add -f build`

`§  git commit -m "forcing build"`

`§ git push`

> **Passo 1.7** - Criar um novo item no Jenkins

- Aceder a http://localhost:8080/
- Selecionar "New item"
- Atribuir um nome o job ca5part1
- Seleccionar "pipeline"
- Escolher a opção Pipeline script from SCM
- Ecolher Git para SCM
- Atribuir o repositório pretendido https://bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769
- Branch : */master
- Script path : CA5/part1/gradle_basic_demo/Jenkinsfile

> **Passo 1.8** - Alterações em build.gradle 

No ficheiro build.gradle foi necessário alterar a dependência do junit para 

    testImplementation 'junit:junit:4.13.1'


> **Passo 1.9** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos:

`§ git add .`

`§ git commit -m "CA5 done `

`§ git push`

`§ git tag ca5-part1`

`§ git push origin ca5-part1`










