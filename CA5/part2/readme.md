>> # DEVOPS

> ## CA5 (class assignment 5)


>### Parte 2


## Jenkins

>**Passo 1.1** - Terminal

Abrir o terminal/linha de comandos da máquina local.


> **Passo 1.2** - Criação do diretório CA5/part1 dentro do diretório /devops-21-22-lmn-1211769

Executar os comandos:

`§ cd devops-21-22-lmn-1211769/CA5` - Este comando dá acesso ao repositório pretendido.

`§ mkdir part2` - Criação de novo diretório part1.


> **Passo 1.3** - Correr o war file diretamente no terminal

``§ java -jar jenkins.war `` (e deixar o terminal a correr para todos os passos seguintes)


> **Passo 1.4** - Aceder ao endereço localhost com o porto definido

http://localhost:8080 


> **Passo 1.5** - Copiar gradle_basic_demo para a pasta CA5/part2 e guardar as alterações no repositório

Copiar a pasta gradle_basic_demo através do comando ``§ cp`` para a pasta CA5/part2

Posteriormente dentro da pasta CA5/part2/gradle_basic_demo executar os comandos:

``§ git add -f gradle-wrapper.jar ``

`§ git commit -m "forcing gradle-wrapper.jar"`

`§ git push`

`§ git add -f build`

`§  git commit -m "forcing build"`

`§ git push`


> **Passo 1.6** - Instalar plugins Javadoc e docker pipeline

- Selecionar "Gerir o Jenkins"
- Selecionar "Gerir plugins"
- Selecionar Disponíveis
- Procurar por "HTML publisher" e instalar o plugin
- Procurar por "Docker pipeline" e instalar o plugin

> **Passo 1.7** - Anexar Dockerfile ao diretório 

Copiar o Dockerfile proveniente do CA4-part1B para este diretório.


> **Passo 1.8** - Criar credenciais de acordo com o DockerHub

- Selecionar "Gerir o Jenkins"
- Selecionar "Manage credentials"
- Slecionar Global
- Adicionar credenciais
- Escrever o username do Docker hub
- Escrever a password do Docker hub
- Escrever um id "dockerhub_credentials"


> **Passo 1.9** - Criar um novo item no Jenkins

- Aceder a http://localhost:8080/
- Selecionar "New item"
- Atribuir um nome o job ca5part2
- Seleccionar "pipeline"
- Escolher a opção Pipeline script from SCM
- Ecolher Git para SCM
- Atribuir o repositório pretendido https://bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769
- Branch : */master
- Script path : CA5/part1/gradle_basic_demo/Jenkinsfile

- Clicar em Build Now e aguardar que todas as stages façam build como esperado.

> **Passo 1.10** - Enviar alterações para o repositório remoto no Bitbucket

Para adicionar as alterações feitas localmente ao repositório do Bitbucket, executar os comandos:

`§ git add .`


`§ git commit -m "CA5 done `

`§ git push`

`§ git tag ca5-part2`

`§ git push origin ca5-part2`



>### Alternativa

Há várias alternativas ao Jenkins: 

- Buddy

- FinalBuilder

- GoCD

- IBM Urbancode

- Circle CI

- TeamCity

- GitLab CI 



Escolhi falar sobre o GitLab por também ser um software open-source como o Jenkins.

>###GitLabCI

####Built-in CI/CD
- O GitLabCI suporta CI(continuous integration) e CD(continuous delivery) tal como o Jenkins.

####Application Performance Monitoring
- O GitLabCI consegue monitorizar todas as apps implementadas por este software. Contrariamente ao Jenkins, 
é possível analisar o impacto de qualquer alteração. No Jenkins apenas é possível verificar info básica
isoladas sobre o código implementado mas não é possível ter uma visão do desempenho geral como no GitLabCI.

####Self-monitoring
-O GitLabCI possui recursos de auto monitorização que facilitam a programação geral na implementação e manutenção.
Apesar de também possuir esta funcionalidade, o Jenkins permite que se agende um "trabalho" mas  é necessário escrever o script
para o efeito através do SCM.


####Application Performance Alert

- O GitLabCI permite criar um serviço de alertas para determinados eventos/"trabalhos" enquanto o Jenkins
não tem esta funcionalidade.


####Built-in Container Registry

- Ao contrário do GitLab, o Jenkins necessita de plugins adicionais para poder fazer upload de 
imagens de um container. O GitLab dispõe dessa funcionalidade na integração contínua.


####Comprehensive Pipeline Graphs

- O Gitlab e o Jenkins permitem aos utilizadores ver através de gráficos pipeline o avanço de cada processo 
e o status atual.


## Implementação da alternativa

- Aceder www.gitlab.com
- Criar conta 
- Clicar em Menu e criar um novo projeto
- Importar projeto
- Selecionar Repository by URL
- Colar o URL https://bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769/
- Clicar em Set up CI/CD


####Escrever o script no GitLab:

O GitLab gera um ficheiro .gitlab-ci.yml onde os únicos stages possíveis são:
- .pre
- build
- test
- deploy
- .post

Para escrever o script de forma a correr a nossa aplicação gradle_basic_demo no GitLab, uma das grandes diferenças 
detetadas (em comparação com o Jenkins) é que:

- o stage Checkout não precisa de ser escrito no script pois o GitLab já dispõe dessa funcionalidade - clonar e
utilizar um repositório existente.

- os stages Generate Jar, Docker Image não necessitam de ser escritos no scripit, pois o GitLab também
dispõe dessa funcionalidade diretamente no seu Dashboard em "Packages & Registries - Container Register"

- para o stage Archiving, o GitLab disponibiliza uma funcionalidade que é os Artifacts.


Infelizmente não me foi possível continuar com a implementação a alternativa, mas de qualquer forma, consegui
perceber que há várias alternativas ao Jenkins (muitas das opções são pagas) e que o GitLab, do que explorei,
parece-me ter uma UI mais user-friendly e mais funcionalidades integradas que evitam scripts mais longos.

Nota: o GitLab CI não é totalmente open-source.








